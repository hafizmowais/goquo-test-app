/**
 * Create the store with dynamic reducers
 */

import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import { routerMiddleware } from 'connected-react-router/immutable';
import createSagaMiddleware from 'redux-saga';
import { reactReduxFirebase } from 'react-redux-firebase';
import firebase from 'firebase';
import createReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();

const config = {
  apiKey: 'AIzaSyC5oNbE_GK7Ghvv5f2qpOixP7z_98dv-tM',
  authDomain: 'goquo-test-app.firebaseapp.com',
  databaseURL: 'https://goquo-test-app.firebaseio.com',
  projectId: 'goquo-test-app',
  storageBucket: 'goquo-test-app.appspot.com',
  messagingSenderId: '541600719843',
};
firebase.initializeApp(config);
window.firebase = firebase;

export default function configureStore(initialState = {}, history) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares = [sagaMiddleware, routerMiddleware(history)];

  const enhancers = [applyMiddleware(...middlewares)];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle, indent */
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
      : compose;
  /* eslint-enable */

  // const store = createStore(
  //   createReducer(),
  //   fromJS(initialState),
  //   composeEnhancers(...enhancers),
  // );
  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(reactReduxFirebase(firebase), ...enhancers),
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {}; // Reducer registry
  store.injectedSagas = {}; // Saga registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers));
    });
  }

  return store;
}
