import React from 'react';
import { Modal, InputNumber, Form, Button } from 'antd';
import * as R from 'ramda';

const FormItem = Form.Item;

export class bookingModal extends React.Component {
  state = {
    roomAllocation: 0,
  };

  checkNumberOfAdults = (rule, value, callback) => {
    const form = this.props.form;
    const total_count = value + form.getFieldValue('input-childerns');
    if (
      form.getFieldValue('input-childerns') != 0 ||
      form.getFieldValue('input-infants') != 0
    ) {
      form.validateFields(['input-childerns', 'input-infants'], {
        force: true,
      });
    }
    if (total_count > 7) {
      callback('Total 7 people is allowed in 1 booking excluding infants');
    } else {
      callback();
    }
  };

  checkNumberOfChildrens = (rule, value, callback) => {
    const form = this.props.form;
    const total_count = value + form.getFieldValue('input-adults');
    if (total_count > 7) {
      callback('Total 7 people is allowed in 1 booking excluding infants');
    } else if (value / form.getFieldValue('input-adults') > 3) {
      callback(`for ${value} childrens you required to have ${Math.ceil(value / 3)} adults`);
    } else {
      callback();
    }
  };

  checkNumberOfInfants = (rule, value, callback) => {
    const form = this.props.form;
    if (value > 9) {
      callback('Total 9 infant is allowed in 1 booking');
    } else if (value / form.getFieldValue('input-adults') > 3) {
      callback(`for ${  value  } infants you required to have ${  Math.ceil(value/3)  } adults`);
    } else {
      callback();
    }
  };

  validateForm = () => {
    const { form, handleOk } = this.props;
    form.validateFields((error, value) => {
      if (!error) {
        const byAdult = Math.ceil(form.getFieldValue('input-adults') / 3);
        const byChildren = Math.ceil(form.getFieldValue('input-childerns') / 3);
        const byInfants = Math.ceil(form.getFieldValue('input-infants') / 3);
        const totalRooms = Math.max(byAdult, byChildren, byInfants);
        this.setState({
          roomAllocation: totalRooms,
        });
        handleOk(value);
      }
    });
  };

  formClear = () => {
    const { form, handleCancel } = this.props;
    handleCancel();
    form.resetFields();
    this.setState({
      roomAllocation: 0,
    });
  };

  render() {
    const { visible, property_data, handleCancel } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { roomAllocation } = this.state;

    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 },
    };

    return (
      <div>
        <Modal
          title="Booking Inormation"
          visible={visible}
          onOk={this.validateForm}
          onCancel={this.formClear}
          footer={[
            <Button key="back" onClick={this.formClear}>
              Cancel
            </Button>,
            <Button key="submit" type="primary" onClick={this.validateForm}>
              Get Room Allocation
            </Button>,
          ]}
        >
          <p>
            <b>Category: </b>
            {R.pathOr(
              'roomTypeLabel is missing',
              ['roomTypeLabel'],
              property_data,
            )}
          </p>
          <p>
            <b>Bed Type: </b>
            {R.pathOr(
              'bedTypeLabel is missing',
              ['bedTypeLabel', '0'],
              property_data,
            )}
          </p>
          <p>
            <b>Facility: </b>
            {R.pathOr(
              'boardCodeDescription is missing',
              ['boardCodeDescription'],
              property_data,
            )}
          </p>
          <p>
            <b>Price: </b>
            {R.pathOr('totalPrice is missing', ['totalPrice'], property_data)}
          </p>
          <Form>
            <FormItem {...formItemLayout} label="Adults">
              {getFieldDecorator('input-adults', {
                initialValue: 1,
                rules: [
                  {
                    type: 'number',
                    message: 'The input is not valid number',
                  },
                  {
                    required: true,
                    message: 'Please input number of adult!',
                  },
                  {
                    validator: this.checkNumberOfAdults,
                  }]
              })(
                <InputNumber min={1} />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Childerns">
              {getFieldDecorator('input-childerns', {
                initialValue: 0,
                rules: [
                  {
                    type: 'number',
                    message: 'The input is not valid number',
                  },
                  {
                    required: true,
                    message: 'Please input number of childerns!',
                  },
                  {
                    validator: this.checkNumberOfChildrens,
                  }]
              })(
                <InputNumber min={0} />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Infants">
              {getFieldDecorator('input-infants', {
                initialValue: 0,
                rules: [
                  {
                    type: 'number',
                    message: 'The input is not valid number',
                  },
                  {
                    required: true,
                    message: 'Please input number of infants!',
                  },
                  {
                    validator: this.checkNumberOfInfants,
                  }]
              })(
                <InputNumber min={0} />
              )}
            </FormItem>
          </Form>

          <p>
            <b>Room Allocation: </b>
            <span className="room-allocation">{roomAllocation} Rooms</span>
          </p>
        </Modal>
      </div>
    );
  }
}

export default Form.create()(bookingModal);
