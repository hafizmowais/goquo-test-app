import React from 'react';
import { Row, Col, Button } from 'antd';
import * as R from 'ramda';

export default class renderHotels extends React.Component {
  groupBy = list => {
    const map = new Map();
    if (list) {
      list.forEach(item => {
        const key = R.pathOr(null, ['roomTypeLabel', '0'], item);
        const collection = map.get(key);
        if (!collection) {
          map.set(key, [item]);
        } else {
          collection.push(item);
        }
      });
    }
    return map;
  };

  properties_html = (data_list, handleOk) => {
    const property_html = [];
    data_list.forEach((property, p_key) => {
      const inner_property_html = [];
      const inner_price_html = [];
      property.forEach((individual_property, ind_key) => {
        inner_property_html.push(
          <p key={ind_key} className="inner-property">
            {R.pathOr(
              'bedTypeLabel is missing',
              ['bedTypeLabel', '0'],
              individual_property,
            )}
            <span className="seperator">-</span>
            {R.pathOr(
              'boardCodeDescription is missing',
              ['boardCodeDescription'],
              individual_property,
            )}
          </p>,
        );
        inner_price_html.push(
          <p key={ind_key} className="inner-price">
            {R.pathOr(
              'totalPrice is missing',
              ['totalPrice'],
              individual_property,
            )}
            <Button
              type="primary"
              onClick={() => handleOk(individual_property)}
              className="book-button"
            >
              Book Now
            </Button>
          </p>,
        );
      });
      property_html.push(
        <Row key={p_key} className="main-row">
          <Col className="first-col" span={6}>
            {p_key}
          </Col>
          <Col className="second-col" span={12}>
            {inner_property_html}
          </Col>
          <Col className="third-col" span={6}>
            {inner_price_html}
          </Col>
        </Row>,
      );
    });
    return property_html;
  };

  render() {
    const { properties, handleOk } = this.props;

    const grouped_properties = this.groupBy(properties);

    return <div>{this.properties_html(grouped_properties, handleOk)}</div>;
  }
}
