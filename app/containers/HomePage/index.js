/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Layout, Menu } from 'antd';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import RenderHotels from './components/renderHotels';
import BookingModal from './components/bookigModal';

import style from './index.css';

const { Header, Content, Footer } = Layout;

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  state = {
    property_data: null,
    visible: false,
  };

  popupShow = record => {
    this.setState({
      property_data: record,
      visible: true,
    });
  };

  handleOk = value => {
    console.log(value);
    // this.setState({
    //   visible: false,
    // });
  };

  handleCancel = () => {
    this.setState({
      property_data: null,
      visible: false,
    });
  };

  render() {
    const { properties } = this.props;
    const { property_data, visible } = this.state;
    return (
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}
          />
        </Header>
        <Content style={{ padding: '50px' }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
            <RenderHotels properties={properties} handleOk={this.popupShow} />
            <BookingModal
              property_data={property_data}
              visible={visible}
              handleOk={this.handleOk}
              handleCancel={this.handleCancel}
            />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Goquo ©2018 All rights reserved.
        </Footer>
      </Layout>
    );
  }
}

const enhanced = compose(
  firebaseConnect(() => [
    {
      path: '/properties',
      storeAs: `properties`,
    },
  ]),
  connect(state => {
    const { data } = state.get('firebase');
    return {
      properties: data && data.properties,
    };
  }),
);
export default enhanced(HomePage);
